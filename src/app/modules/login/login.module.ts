import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
//import { MdButtonModule, MdCheckboxModule, MdGridListModule, MdInputModule, MdIconModule } from 'angular-material';

import { MatButtonModule,MatTableModule,MatDatepickerModule, MatNativeDateModule,MatCheckboxModule, MatGridListModule, MatInputModule, MatIconModule } from '@angular/material';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule, MatButtonModule,MatDatepickerModule, MatNativeDateModule,
    MatCheckboxModule, MatGridListModule, MatInputModule, MatIconModule,MatTableModule
  ],
  declarations: [LoginComponent],
  exports: [LoginComponent]
})
export class LoginModule { }
